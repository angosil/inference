import pytest

from src.domain.entities import UserExperianData, EntityValidationError, UserData


def test_experian_box_creation(experian_box_data):
    experian_box = UserExperianData(**experian_box_data)
    assert experian_box.experian_score == 742
    assert experian_box.experian_score_probability_default == 0.93
    assert experian_box.experian_score_percentile == 6
    assert experian_box.experian_mark == 'H'


def test_experian_box_invalid_experian_score():
    with pytest.raises(EntityValidationError):
        UserExperianData(2001, 0.5, 50, 'D')


def test_experian_box_invalid_experian_score_probability_default():
    with pytest.raises(EntityValidationError):
        UserExperianData(500, 1.5, 50, 'D')


def test_experian_box_invalid_experian_score_percentile():
    with pytest.raises(EntityValidationError):
        UserExperianData(500, 0.5, 150, 'D')


def test_experian_box_invalid_experian_mark():
    with pytest.raises(EntityValidationError):
        UserExperianData(500, 0.5, 50, 'Z')


def test_user_data_creation(experian_box_data, dni):
    experian_data = UserExperianData(**experian_box_data)
    user_data = UserData(dni, experian_data)
    assert user_data.dni == dni
    assert user_data.experian_data == experian_data


def test_user_data_invalid_dni(experian_box_data):
    invalid_dni = "12345678A"
    experian_data = UserExperianData(**experian_box_data)
    with pytest.raises(EntityValidationError):
        UserData(invalid_dni, experian_data)
