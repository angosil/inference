import pytest
from sklearn.pipeline import Pipeline

from src.applications.prediction_service import PredictionService, load_model, ModelLoadingError
from src.config import Config
from src.domain.entities import UserExperianData


@pytest.fixture
def model_path():
    """Fixture to return the path to the model file"""
    return Config.MODEL_PATH


def test_load_model(model_path):
    """Test that the model is loaded correctly"""
    model = load_model(model_path)
    assert isinstance(model, Pipeline), "Model is not a Pipeline object"


def test_load_model_invalid_path():
    """Test that the model is loaded correctly"""
    with pytest.raises(ModelLoadingError):
        load_model("invalid_path")


@pytest.fixture
def invalid_model_path():
    """Fixture to return the path to the model file"""
    return "fixture_data/applications/invalid_model.pkl"


def test_load_model_invalid_model(invalid_model_path):
    """Test that the model is loaded correctly"""
    with pytest.raises(ModelLoadingError):
        load_model(invalid_model_path)


@pytest.fixture
def input_data(experian_box_data):
    """Fixture to return the input data for the model"""
    return UserExperianData(**experian_box_data)


def test_prediction_service(input_data, model_path):
    prediction_service = PredictionService(model_path)
    prediction = prediction_service.predict(input_data)
    assert prediction == 3
