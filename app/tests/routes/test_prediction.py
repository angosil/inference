def test_prediction(client, dni, experian_box_data):
    response = client.post(f'/prediction/{dni}', json=experian_box_data)
    assert response.status_code == 200
    json_data = response.get_json()
    assert json_data == {'asnef_score': 3}


def test_prediction_invalid_dni(client, experian_box_data):
    response = client.post('/prediction/12345678', json=experian_box_data)
    assert response.status_code == 400
    json_data = response.get_json()
    assert json_data == {'error': 'Bad Request', 'message': 'Invalid DNI'}


def test_prediction_invalid_body(client, dni):
    response = client.post(f'/prediction/{dni}')
    assert response.status_code == 400
    json_data = response.get_json()
    assert json_data == {'error': 'Bad Request', 'message': 'Body required'}


def test_prediction_invalid_experian_score(client, dni, experian_box_data):
    experian_box_data['experian_score'] = None
    response = client.post(f'/prediction/{dni}', json=experian_box_data)
    assert response.status_code == 400
    json_data = response.get_json()
    assert json_data == {'error': 'Bad Request', 'message': 'Experian score required'}


def test_prediction_invalid_experian_score_probability_default(client, dni, experian_box_data):
    experian_box_data['experian_score_probability_default'] = None
    response = client.post(f'/prediction/{dni}', json=experian_box_data)
    assert response.status_code == 400
    json_data = response.get_json()
    assert json_data == {'error': 'Bad Request', 'message': 'Experian score probability default required'}


def test_prediction_invalid_experian_score_percentile(client, dni, experian_box_data):
    experian_box_data['experian_score_percentile'] = None
    response = client.post(f'/prediction/{dni}', json=experian_box_data)
    assert response.status_code == 400
    json_data = response.get_json()
    assert json_data == {'error': 'Bad Request', 'message': 'Experian score percentile required'}


def test_prediction_invalid_experian_mark(client, dni, experian_box_data):
    experian_box_data['experian_mark'] = None
    response = client.post(f'/prediction/{dni}', json=experian_box_data)
    assert response.status_code == 400
    json_data = response.get_json()
    assert json_data == {'error': 'Bad Request', 'message': 'Experian mark required'}


def test_prediction_invalid_experian_score_value(client, dni, experian_box_data):
    experian_box_data['experian_score'] = 2001
    response = client.post(f'/prediction/{dni}', json=experian_box_data)
    assert response.status_code == 400
    json_data = response.get_json()
    assert json_data == {'error': 'Bad Request', 'message': 'experian_score must be between 0 and 2000'}


def test_prediction_invalid_experian_score_probability_default_value(client, dni, experian_box_data):
    experian_box_data['experian_score_probability_default'] = 1.5
    response = client.post(f'/prediction/{dni}', json=experian_box_data)
    assert response.status_code == 400
    json_data = response.get_json()
    assert json_data == {'error': 'Bad Request',
                         'message': 'experian_score_probability_default must be between 0 and 1'}


def test_prediction_invalid_experian_score_percentile_value(client, dni, experian_box_data):
    experian_box_data['experian_score_percentile'] = 150
    response = client.post(f'/prediction/{dni}', json=experian_box_data)
    assert response.status_code == 400
    json_data = response.get_json()
    assert json_data == {'error': 'Bad Request', 'message': 'experian_score_percentile must be between 0 and 100'}


def test_prediction_invalid_experian_mark_value(client, dni, experian_box_data):
    experian_box_data['experian_mark'] = 'Z'
    response = client.post(f'/prediction/{dni}', json=experian_box_data)
    assert response.status_code == 400
    json_data = response.get_json()
    assert json_data == {'error': 'Bad Request', 'message': 'experian_mark must be a character from A to H'}
