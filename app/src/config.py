import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    API_VERSION = "v1.0"

    SECRET_KEY = os.getenv("APP_SECRET_KEY", "my_secret")

    LOG_TYPE = os.getenv("APP_LOG_TYPE", "stream")  # ["watched", "stream"]
    LOG_LEVEL = os.getenv("APP_LOG_LEVEL", "DEBUG")  # ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]

    MODEL_PATH = 'src/applications/asnef_inference.pkl'
