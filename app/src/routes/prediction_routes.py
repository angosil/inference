from typing import Dict

from flask import Flask, request, jsonify
from werkzeug.exceptions import BadRequest

from src.applications.prediction_service import PredictionService
from src.config import Config
from src.domain.entities import EntityValidationError, UserExperianData, UserData


class Prediction:
    def __init__(self, app: Flask = None):
        self.app = app

        self.app.add_url_rule('/prediction/<dni>', view_func=self.prediction, methods=['POST'])

    @staticmethod
    def prediction(dni: str):
        """this end-pont will return a prediction based on the model

        Returns:
            (json) the prediction, the asnef score
        """
        body = required(request.get_json(silent=True), "Body required")
        experian_score = required(body.get('experian_score'), "Experian score required")
        experian_score_probability_default = required(body.get('experian_score_probability_default'),
                                                      "Experian score probability default required")
        experian_score_percentile = required(body.get('experian_score_percentile'),
                                             "Experian score percentile required")
        experian_mark = required(body.get('experian_mark'), "Experian mark required")
        try:
            entity = UserExperianData(
                experian_score=experian_score,
                experian_score_probability_default=experian_score_probability_default,
                experian_score_percentile=experian_score_percentile,
                experian_mark=experian_mark
            )
            user_data = UserData(dni, entity)
            prediction = PredictionService(Config.MODEL_PATH).predict(user_data.experian_data)
            return jsonify(asnef_score=prediction)
        except EntityValidationError as error:
            raise BadRequest(str(error))


def required(param: [Dict, str], message: str) -> Dict or str:
    """This function is to raise BadRequest with custom message if some data is None.

    Args:
        param:
        message:

    Returns:
        the data.

    Raise:
        BadRequest if data is None.
    """
    if param is not None:
        return param
    raise BadRequest(message)
