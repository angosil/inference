from flask import Flask

from src.routes.prediction_routes import Prediction
from src.routes.welcome_routes import WelcomeRoutes


def load_routes(app: Flask):
    """

    Args:
        app:

    Returns:

    """
    WelcomeRoutes(app)
    Prediction(app)
