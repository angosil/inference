import logging

from flask import jsonify, Flask

from src.config import Config


class WelcomeRoutes:

    def __init__(self, app: Flask = None):
        self._app = app

        app.add_url_rule('/', view_func=self.welcome, methods=['GET'])

    @staticmethod
    def welcome():
        """Welcome endpoint

        Returns:
            (json) a welcome message
        """
        logging.info("Calling welcome endpoint")
        version = Config.API_VERSION
        return jsonify(status="success",
                       message=f'Welcome to Base microservice {version}')
