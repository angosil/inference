import os
import pickle

import pandas as pd

from src.domain.entities import UserExperianData


class ServiceError(Exception):
    """Base class for exceptions in this module."""
    pass


class ModelLoadingError(ServiceError):
    """Exception raised when the model cannot be loaded."""

    def __init__(self, message: str = None):
        self.message = message if message else "Failed to load the model"
        super().__init__(self.message)


def load_model(model_path):
    if not os.path.exists(model_path):
        raise ModelLoadingError(f"The model file {model_path} does not exist")
    try:
        with open(model_path, 'rb') as file:
            model = pickle.load(file)
        return model
    except Exception as e:
        raise ModelLoadingError() from e


class PredictionService:
    """This class will be used to make predictions using the model"""

    def __init__(self, model_path: str):
        """Initialize the class with the model

        Args:
            model_path: The path to the model file
        """
        self.model = load_model(model_path)

    def predict(self, experian_box: UserExperianData) -> int:
        """Make a prediction using the model

        Args:
            experian_box (UserExperianData): The ExperianBox object containing the input data

        Returns:
            (int) the prediction
        """
        input_data = pd.DataFrame(
            [[experian_box.experian_score, experian_box.experian_score_probability_default,
              experian_box.experian_score_percentile, experian_box.experian_mark]],
            columns=['experian-score', 'experian-score_probability_default', 'experian-score_percentile',
                     'experian-mark']
        )
        raw_prediction = self.model.predict(input_data)

        # Post-process the raw prediction to ensure it's an integer between 1 and 6
        processed_prediction = max(1, min(6, round(raw_prediction[0])))

        return processed_prediction
