from flask import json
from werkzeug.exceptions import HTTPException


def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "error": e.name,
        "message": e.description,
    })
    response.content_type = "application/json"
    return response


def register_error(app):
    app.register_error_handler(HTTPException, handle_exception)
