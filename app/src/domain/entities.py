class EntityError(Exception):
    """Custom exception for entity validation errors"""
    pass


class EntityValidationError(EntityError):
    """Custom exception for entity validation errors"""

    def __init__(self, message: str = None):
        self.message = message if message else "Entity validation failed"
        super().__init__(self.message)


class UserExperianData:
    """This class is used to store the experian data of a user. It also has validation for the data."""

    def __init__(self, experian_score: int, experian_score_probability_default: float,
                 experian_score_percentile: float, experian_mark: str):
        self.experian_score = experian_score
        self.experian_score_probability_default = experian_score_probability_default
        self.experian_score_percentile = experian_score_percentile
        self.experian_mark = experian_mark

    @property
    def experian_score(self):
        return self._experian_score

    @experian_score.setter
    def experian_score(self, value):
        if not 0 <= value <= 2000:
            raise EntityValidationError("experian_score must be between 0 and 2000")
        self._experian_score = value

    @property
    def experian_score_probability_default(self):
        return self._experian_score_probability_default

    @experian_score_probability_default.setter
    def experian_score_probability_default(self, value):
        if not 0 <= value <= 1:
            raise EntityValidationError("experian_score_probability_default must be between 0 and 1")
        self._experian_score_probability_default = value

    @property
    def experian_score_percentile(self):
        return self._experian_score_percentile

    @experian_score_percentile.setter
    def experian_score_percentile(self, value):
        if not 0 <= value <= 100:
            raise EntityValidationError("experian_score_percentile must be between 0 and 100")
        self._experian_score_percentile = value

    @property
    def experian_mark(self):
        return self._experian_mark

    @experian_mark.setter
    def experian_mark(self, value):
        if value not in list('ABCDEFGH'):
            raise EntityValidationError("experian_mark must be a character from A to H")
        self._experian_mark = value


class UserData:
    def __init__(self, dni: str, experian_data: UserExperianData):
        self.dni = dni
        self.experian_data = experian_data

    @property
    def dni(self):
        return self._dni

    @dni.setter
    def dni(self, value):
        if not self.validate_dni(value):
            raise EntityValidationError("Invalid DNI")
        self._dni = value

    @staticmethod
    def validate_dni(dni):
        if len(dni) != 9:
            return False
        letter = dni[-1]
        digits = dni[:-1]
        if not digits.isdigit():
            return False
        letters = "TRWAGMYFPDXBNJZSQVHLCKE"
        calculated_letter = letters[int(digits) % 23]
        return calculated_letter == letter.upper()
