import logging
import sys

from src.config import Config


def setup():
    root = logging.getLogger()
    root.setLevel(Config.LOG_LEVEL)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('[%(asctime)s] %(levelname)s in %(pathname)s:%(funcName)s: %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)
