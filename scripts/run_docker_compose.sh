#!/bin/bash
# Version: 1.1.0
# Date: 2023-05-15
# Source: dockerize-vuexy-application
# Objective:
# Description:
# ChangeLog:

if [ "$1" = "prod" ]; then
  COMPOSE_FILE="docker-compose.prod.yaml"
else
  COMPOSE_FILE="docker-compose.yaml"
fi

remove_docker_images() {
  echo "Removing docker images"
  docker compose -f "$COMPOSE_FILE" down --rmi all -v
}

if [[ "$1" = "-r"  || "$1" = "--restart" || "$2" = "-r"  || "$2" = "--restart" ]]; then
  echo
  echo "1) Optional project restart"
  remove_docker_images
fi

echo
echo "2) Starting docker containers"
docker compose -f "$COMPOSE_FILE" up --build -d || exit

echo
echo "3) Waiting for the app service"
while ! curl http://127.0.0.1:7272 -m1 -o/dev/null -s ; do
  echo "Waiting ..."
  sleep 1
done

echo
echo "4) Services status"
docker compose -f "$COMPOSE_FILE" ps

echo
echo "5) Showing logs"
docker compose -f "$COMPOSE_FILE" logs app

echo
echo "App is up"