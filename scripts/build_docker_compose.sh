#!/bin/bash
# Version: 1.1.0
# Date: 2023-05-15
# Source: dockerize-vuexy-application
# Objective:
# Description:
# ChangeLog:

COMPOSE_FILE="docker-compose.build.yaml"

remove_docker_images() {
  echo "Removing docker images"
  docker-compose -f $COMPOSE_FILE down --rmi all -v
}

if [[ $1 = "-r"  || $1 = "--restart" ]]; then
  echo
  echo "1) Optional project restart"
  remove_docker_images
fi

echo
echo "2) Starting docker containers"
docker-compose -f $COMPOSE_FILE up --build -d || exit

echo
echo "3) Install all python libraries ..."
docker-compose -f $COMPOSE_FILE exec -w /project/app build pip install -r requirements.ci.txt

echo
echo "4) Flake8 ..."
docker-compose -f $COMPOSE_FILE exec -w /project/app build flake8 src tests --format=html --htmldir=flake-report || exit

echo
echo "5) Tests and coverage ..."
docker-compose -f $COMPOSE_FILE exec -w /project/app build coverage run -m pytest -vv tests/ || exit
docker-compose -f $COMPOSE_FILE exec -w /project/app build coverage html
docker-compose -f $COMPOSE_FILE exec -w /project/app build coverage xml

echo
echo "6) documentation ..."
docker-compose -f $COMPOSE_FILE exec -w /project/docs build make html

if  [[ $1 = "-d" ]]; then
    echo "7) Optional project deleting containers and images"
    remove_docker_images
fi

echo "8) Checking running images"
docker-compose -f docker-compose.build.yaml ps
