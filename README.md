# inference

This readme file is to provide a brief overview of the project and instructions for setting up and using the API REST. 

## How to Use the API REST

A service that allows us to obtain the risk of non-payment from a customer. This application uses multiple third-party 
sources to function, including calls to different credit bureaus (ASNEF and Experian). The API REST microservice is a 
Flask application that provides endpoints for performing inference on a machine learning model. The model is to infer 
the ASNEF score based on the Experian data.

The API REST microservice provides the following endpoints:

* WELCOME: ``GET /`` This endpoint returns a welcome message. Is useful to check if the service is running.
* PREDICTION: ``POST /prediction/[dni]`` This endpoint receives a JSON payload with the Experian data and returns the ASNEF score.

The dni parameter is a string that represents the customer's identification number.

The Experian data is a JSON object with the following structure:

```json
{
  "experian_score": "int between 0 and 2000",
  "experian_score_probability_default": "float between 0 and 1",
  "experian_score_percentile": "float between 0 and 100",
  "experian_mark": "char between A and H"
}
```
The response from the PREDICTION endpoint is a JSON object with the following structure:

```json
{"asnef_score": "int between 1 and 6"}
```

### Coming Soon

The API REST microservice will include the following endpoints in the future:

* REGISTER: ``GET /register/[dni]`` This endpoint returns customer calling history.

## Deployment

The API REST microservice is deployed using Docker. The project builds a Docker image for the application [inference](https://hub.docker.com/r/angosil/inference)
you can deploy the application using the following command using docker compose:

### development environment

1 - Crete the compose.yaml file with the following content:

```yaml
version: '3.8'

services:
  app:
    image: angosil/inference
    command: python manage.py run -h 0.0.0.0 -p 7272
    restart: always
    volumes:
      - .:/home/ana
    ports:
      - '7272:7272'
    env_file:
      - .env
```

2- Then create the ``.env`` file with the following content:

```shell
APP_SECRET_KEY=o7yzszrqerihg9bc
FLASK_APP=src/app.py
FLASK_ENV=development
```

3 - Then run the following command:

```shell
docker compose -f compose.yaml up --build -d
```

The application will be available at http://localhost:7272

### production environment

You can also deploy the application for production using nginx and gunicorn, you can create this docker-compose.yaml file:

```yaml
version: '3.8'

services:
  app:
    image: angosil/inference
    command: gunicorn --bind 0.0.0.0:5000 manage:app
    expose:
      - 5000
    env_file:
      - ./.env
  nginx:
    image: nginx:1.17-alpine
    ports:
      - '7272:80'
    volumes:
      - ./nginx:/etc/nginx/conf.d  # Change this line
    depends_on:
      - app
```

Then create nginx directory and the nginx.conf file with the following content:

```shell
upstream api-server {
    server app:5000;
}

server {

    listen 80;

    location / {
        proxy_pass http://api-server;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_redirect off;
    }

}
```

Then create the ``.env`` file with the following content:

```shell
APP_SECRET_KEY=o7yzszrqerihg9bc
FLASK_APP=src/app.py
FLASK_ENV=production
```

Then run the following command:

```shell
docker compose -f compose.yaml up --build -d
```

The application will be available at http://localhost:7272

### Helm Chart

In the future the project will include a Helm chart for deploying the API REST microservice to a Kubernetes cluster.

## Setup For development

To set up and run the API REST microservice project, please follow these steps:

1. Clone the GitLab repository to your local machine.
2. Make sure you have Docker and Docker Compose installed.
3. Navigate to the project's root directory.

### Development Environment

You have the option to use the `docker-compose` command directly or use the provided scripts for starting a development environment based on Docker Compose.

- Use the `run_docker_compose.sh` script to start a development environment or a production-like environment.
- Use the `build_docker_compose.sh` script for creating an isolated environment to run sequential steps similar to the GitLab CI pipeline.

Please make sure the scripts are executable by running the following command:

```shell
chmod +x scripts/run_docker_compose.sh
chmod +x scripts/build_docker_compose.sh
chmod +x scripts/hard_clean.sh
```

To start the development environment using the ``run_docker_compose.sh`` script, run the appropriate command:

```bash
./scripts/run_docker_compose.sh       # Start development environment
./scripts/run_docker_compose.sh prod  # Start production-like environment
```

The script automatically selects the appropriate Docker Compose configuration file based on the provided parameter 
(prod for production-like environment). If no parameter is provided, the script starts the development environment 
using docker-compose.yaml file.

To create an isolated environment and execute sequential steps using the build_docker_compose.sh script, run the 
following command:

```bash
./scripts/build_docker_compose.sh
```

To perform a hard clean of your Docker environment, use the hard_clean.sh script. This script stops all running 
containers, deletes all containers, images, volumes, and networks. Run the following command:

```bash
./scripts/hard_clean.sh
```

Please refer to the script files in the scripts directory for more details on their functionalities.

## Project Structure

The project follows the following directory structure:

```bash
.
├── app
│   ├── coverage.xml
│   ├── Dockerfile
│   ├── Dockerfile.prod
│   ├── entrypoint.prod.sh
│   ├── entrypoint.sh
│   ├── flake-report
│   ├── htmlcov
│   ├── manage.py
│   ├── src
│   ├── requirements.ci.txt
│   ├── requirements.txt
│   ├── test_newman
│   └── tests
├── docker-compose.build.yaml
├── docker-compose.prod.yaml
├── docker-compose.yaml
├── helm
├── LICENSE
├── nginx
│   ├── Dockerfile
│   └── nginx.conf
├── README.md
└── scripts
    ├── build_docker_compose.sh
    ├── hard_clean.sh
    └── run_docker_compose.sh
```

- The ``app`` directory contains the main application code and related files.
- ``Dockerfile`` and ``Dockerfile.prod`` are Docker configuration files for building development and production images, 
  respectively.
- The ``entrypoint.sh`` and ``entrypoint.prod.sh`` scripts are used as entry points for the Docker containers.
- ``manage.py`` is the entry point script for running the Flask application.
- The ``src`` directory contains the project-specific code.
- ``requirements.txt`` and ``requirements.ci.txt`` list the Python dependencies for the project in development and CI 
  environments, respectively.
- The ``tests`` directory holds the unit tests for the application.
- The ``docker-compose.yaml`` and ``docker-compose.prod.yaml`` files are used to define the Docker Compose configurations for 
  development and production environments.
- The docker-compose.build.yaml file specifies the Docker Compose configuration to create an isolated environment ideal 
  fot testing and development.
- The nginx directory contains the Docker configuration for an Nginx server.
- The helm directory is for Helm chart files (if applicable).
- The scripts directory includes various utility scripts for building and running Docker Compose configurations.
